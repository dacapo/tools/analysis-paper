all:	paper.tex
	pdflatex paper.tex; bibtex paper; pdflatex paper.tex

clean:
	find . -type f -name "paper.*" ! -name "paper.tex" ! -name "paper.bib" -delete
	find . -type f -name "*~" -delete
