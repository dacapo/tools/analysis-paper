#!/bin/bash

bms=( 'avrora' 'fop' 'h2' 'luindex' 'lusearch' 'pmd' 'sunflow' 'xalan')

metric=( 'wmc' 'dit' 'noc' 'cbo' 'rfc' 'lcom' 'ca' 'npm')

for j in "${metric[@]}"
do
  echo "\multicolumn{1}{|c|}{${j}}"
  for i in "${bms[@]}"
  do
    eval "grep ${j} csv/static_analysis_executed/*_static_analysis_${i}_mean.csv |  sed -e \"s/.*,//\" |  sed -e \"s/\([0-9]\+\(\.[0-9]\{0,3\}\)\)\(.*\)/\1/g\" | sed -e \"s/^/\& /\""
  done
done
